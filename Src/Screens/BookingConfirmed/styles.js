import { StyleSheet, Dimensions } from 'react-native';
import { colors } from '../../Resource/Colors';
import { isDarkTheme } from '../../Utilities/helper';
import { Fonts } from '../../Resource/Fonts';
const { height, width } = Dimensions.get("screen")

export default styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
    },
    txtBookingConfirmed: {
        fontFamily: Fonts.ApexNewMedium,
        fontWeight: "600",
        fontSize: 20,
        lineHeight: 25,
        letterSpacing: 0.2,
        color: colors.white,
        textAlign:'center',
        marginTop:30
    },
    txtShareLabel:{
        fontFamily: Fonts.Arial,
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 19,
        letterSpacing: 0.2,
        color: colors.white,
        textAlign:'center'
    },
    roundView:{
        height:260,
        width:260,
        alignSelf:'center',
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:colors.textInputBackGroundDark,
        borderRadius:130,
        marginVertical:33
    },
    txtCode:{
        fontFamily: Fonts.Arial,
        fontWeight: "700",
        fontSize: 60,
        lineHeight: 60,
        letterSpacing: 13,
        color: colors.white,
    },
    imgCycleStyle:{
        width:width,
        height:'50%'

    },
    bottomView:{
        position:"absolute",
        bottom:0,
        backgroundColor:colors.textInputBackGroundDark,
        alignItems:'center',
        justifyContent:'center',
        borderTopWidth:2,
        borderColor:colors.horizontalLine,
        width:width,
        paddingHorizontal:25,
        paddingVertical:20
    },
    txtStyle: {
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: colors.black,
        letterSpacing: 0.4,
    },
    txtCancel:{
        fontFamily:Fonts.ApexNew,
        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: colors.white,
        letterSpacing: 0.2,
        opacity:0.5,
        textAlign:"center",
        marginTop:20,
    },
    roundViewBtn: {
        height: 60,
        borderRadius: 40,
        backgroundColor: colors.yellow,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        width:'100%',

    },
})