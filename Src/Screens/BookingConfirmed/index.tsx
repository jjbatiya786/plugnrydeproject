import React, { useEffect, useState } from 'react';
import { Dimensions, Image, SafeAreaView, Text, TouchableOpacity, View } from 'react-native';
import styles from './styles';
import { CommonString, NavStrings } from '../../Resource/Strings';
import { images } from '../../Resource/Images';
import RoundButton from '../../Component/RoundButton';

const BookingConfirmed = (props: any) => {
    const onCancel = () => {

    }
    const onPress = () => {
        props.navigation.reset({
            index: 0,
            routes: [{ name: NavStrings.tabNavigation }]
        })
    }
    return (
        <SafeAreaView style={styles.mainView}>
            <Text style={styles.txtBookingConfirmed}>{CommonString.bookingConfirmed}</Text>
            <Text style={styles.txtShareLabel}>{CommonString.shareLabel1}</Text>
            <View style={styles.roundView}>
                <Text style={styles.txtCode}>{"2346"}</Text>
            </View>
            <Image style={styles.imgCycleStyle} source={images.imgCycleLight1} resizeMode={"contain"} />
            <View style={styles.bottomView}>
                <RoundButton
                    style={styles.roundViewBtn}
                    txt={CommonString.bookAnotherRyde}
                    txtStyle={styles.txtStyle}
                    onPress={onPress}
                />
                <Text onPress={onCancel} style={styles.txtCancel}>{CommonString.cancelTheBooking}</Text>
            </View>
        </SafeAreaView>
    )
}
export default BookingConfirmed;