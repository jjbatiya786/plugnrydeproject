import { StyleSheet } from 'react-native';
import { colors } from '../../Resource/Colors';
import { isDarkTheme } from '../../Utilities/helper';
import { Fonts } from '../../Resource/Fonts';

export default styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
    },
    imgLogo: {
        width: 46,
        height: 31,
    },
    headerView: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'space-between',
        marginTop: 35
    },
    txtTitle: {
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 30,
        lineHeight: 30,
        fontWeight: "500",
        color: isDarkTheme ? colors.yellow : colors.black,
        marginTop: 50,
        marginBottom: 10
    },
    txtContent: {
        fontFamily:Fonts.Arial,
        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: isDarkTheme ? colors.white : colors.black,
        letterSpacing: 0.2,
    },

    roundView: {
        width: 160,
        height: 60,
        borderRadius: 40,
        backgroundColor: colors.sendOtpBtnColor,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        position: "absolute",
        top: -60,
    },
    txtStyle: {
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: 'rgba(0, 0, 0, 0.4)',
        letterSpacing: 0.4,
    },
    txtChange: {
        fontFamily:Fonts.ApexNew,
        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: colors.blue,
        letterSpacing: 0.4,
    },
    textInputContainer: {
        alignSelf: "center",
        marginTop: 30,
    },
    roundedTextInput: {
        fontFamily:Fonts.Arial,

        borderRadius: isDarkTheme ? 29 : 4,
        borderWidth: isDarkTheme ? 1 : 1,
        fontSize: 18,
        lineHeight: 25,
        fontWeight: "400",
        color: isDarkTheme ? colors.white : colors.black,
        letterSpacing: 0.2,
        backgroundColor: isDarkTheme ? colors.textInputBackGroundDark : colors.white,
    },
    txtResendOtp: {
        fontFamily:Fonts.ApexNewMedium,

        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: colors.blue,
        letterSpacing: 0.4,
        textAlign:"center",
        top:40
    },
})