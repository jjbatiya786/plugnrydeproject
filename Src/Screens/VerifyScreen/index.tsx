import React, { useEffect, useRef, useState } from 'react';
import { Image, ImageBackground, SafeAreaView, Text, TextInput, View, } from 'react-native';
import styles from './styles';
import { images } from '../../Resource/Images';
import { CommonString, NavStrings } from '../../Resource/Strings';
import { colors } from '../../Resource/Colors';
import { isDarkTheme } from '../../Utilities/helper';
import LinearGradient from 'react-native-linear-gradient';
import RoundButton from '../../Component/RoundButton';
import OTPTextView from 'react-native-otp-textinput';

const VerifyScreen = (props:any) => {
    const [number, setNumber] = useState("")
    const input = useRef<OTPTextView>(null);

    const onPress=()=>{
        props.navigation.navigate(NavStrings.tabNavigation)
    }
    return (
        <SafeAreaView style={styles.mainView}>
            <View style={{ paddingHorizontal: 20 }}>
                <View style={styles.headerView}>
                    <Image style={styles.imgLogo}
                        source={images.appIcon}
                        resizeMode={'contain'} />
                </View>
                <Text style={styles.txtTitle}>{CommonString.verifyNumber}</Text>
                <Text style={styles.txtContent}>{CommonString.verifyDesc + "+91-8460815228"}
                    <Text style={styles.txtChange}>{CommonString.strChange}</Text>
                </Text>
                <OTPTextView
                    handleTextChange={setNumber}
                    containerStyle={styles.textInputContainer}
                    textInputStyle={styles.roundedTextInput}
                    inputCount={4}
                    inputCellLength={1}
                    tintColor={isDarkTheme ? colors.textInputBackGroundDark : colors.black}
                    offTintColor={isDarkTheme ? colors.textInputBackGroundDark : colors.black}
                />
            </View>

            <ImageBackground
                source={isDarkTheme ? images.mapImageDark : images.mapImageLight}
                style={{ flex: 1, marginTop: 80 }}
            >
                <Text style={styles.txtResendOtp}>{CommonString.resendOTP}</Text>

                <LinearGradient
                    colors={isDarkTheme ? colors.darkShadowLoginScreen : colors.lightShadowLoginScreen}
                    style={{ flex: 1 }}
                />
                <RoundButton
                    style={number.trim().length != 4 ? styles.roundView : [styles.roundView, { backgroundColor: colors.yellow }]}
                    txt={CommonString.verifyBtn}
                    txtStyle={number.trim().length != 4 ? styles.txtStyle : [styles.txtStyle, { color: colors.black }]}
                    onPress={onPress}
                />
            </ImageBackground>
        </SafeAreaView>
    )
}
export default VerifyScreen;