import { StyleSheet,Dimensions } from 'react-native';
import { colors } from '../../Resource/Colors';
import { isDarkTheme } from '../../Utilities/helper';
import { Fonts } from '../../Resource/Fonts';
const { height, width } = Dimensions.get("screen")

export default styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
    },
    //render styles
    roundView1: {
        height: 50, width: 50, borderRadius: 25, backgroundColor: colors.lightYellow,
        alignItems:'center',justifyContent:'center',
        marginRight:10
    },
    roundView2:{
        height: 30, width: 30, borderRadius: 15, backgroundColor: colors.yellow,
        borderWidth:1,
        borderColor:colors.roundBorderColor,
        alignItems:'center',
        justifyContent:'center'
    },
    txtId:{
        fontFamily:Fonts.Arial,
        fontSize:18,
        fontWeight:"700",
        color:colors.black
    },
    horizontalLine: {
        height: 1,
        backgroundColor: isDarkTheme ? colors.horizontalLineDarkColor : colors.horizontalLineLightColor,
        width: width,
        marginVertical:20
    },
    txtPlace:{
        fontFamily:Fonts.ApexNewMedium,
        fontSize:18,
        fontWeight:"600",
        color:isDarkTheme?colors.white:colors.black,
        lineHeight:25,
        letterSpacing:0.2,
    
    },
    txtAddress:{
        fontFamily:Fonts.ApexNew,
        fontSize:16,
        fontWeight:"500",
        color:isDarkTheme?colors.white:colors.black,
        lineHeight:25,
        letterSpacing:0.2,
    },
    imgPlace:{
        width:14,
        height:20,
        tintColor:isDarkTheme?colors.green:undefined,
    },
    txtNearTag:{
        fontFamily:Fonts.Arial,
        fontSize:14,
        fontWeight:"400",
        color:isDarkTheme?colors.white:colors.black,
        lineHeight:25,
        letterSpacing:0.2,
    },
    placeView:{
         flex: 0.2, alignItems: 'center', justifyContent: 'flex-start' ,marginTop:5
    }
})