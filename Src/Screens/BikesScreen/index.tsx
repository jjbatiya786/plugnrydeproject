import React, { useEffect, useState } from 'react';
import { FlatList, Image, SafeAreaView, Text, View } from 'react-native';
import HeaderView from '../../Component/HeaderView';
import { CommonString } from '../../Resource/Strings';
import styles from './styles';
import { images } from '../../Resource/Images';
const dummyData = [
    {
        placeName: 'Spacion Towers',
        address: "Next to Westin Hotel, HITEC City",
        nearTag: '200 m',
        id: 6,
    },
    {
        placeName: 'Knowledge City',
        address: "Next to Westin Hotel, HITEC City",
        nearTag: '500 m',
        id: 8,
    },
    {
        placeName: 'iCare Academy',
        address: "Next to Westin Hotel, HITEC City ",
        nearTag: '1 km',
        id: 3,
    },
    {
        placeName: 'Casbah',
        address: "Next to Westin Hotel, HITEC City",
        nearTag: '1.2 km',
        id: 4,
    },
    {
        placeName: 'Knowledge City',
        address: "Next to Westin Hotel, HITEC City",
        nearTag: '500 m',
        id: 8,
    },
    {
        placeName: 'iCare Academy',
        address: "Next to Westin Hotel, HITEC City",
        nearTag: '1 km',
        id: 3,
    },
    {
        placeName: 'Casbah',
        address: "Next to Westin Hotel, HITEC City",
        nearTag: '1.2 km',
        id: 4,
    }
]
const BikesScreen = () => {
    const renderView = ( item:any) => {
        return (
            <View style={{ flexDirection: 'row'}}>
                <View style={styles.roundView1}>
                    <View style={styles.roundView2}>
                        <Text style={styles.txtId}>{item.item.id}</Text>
                    </View>
                </View>
                <View style={{ flex: 0.8}}>
                    <Text style={styles.txtPlace}>{item.item.placeName}</Text>
                    <Text style={styles.txtAddress}>{item.item.address}</Text>
                </View>
                <View style={styles.placeView}>
                    <Image style={styles.imgPlace} source={images.placeIcon} />
                    <Text style={styles.txtNearTag}>{item.item.nearTag}</Text>
                </View>
            </View>
        )
    }
    const renderSeparate = () => {
        return (
            <View style={styles.horizontalLine} />
        )
    }
    return (
        <SafeAreaView style={styles.mainView}>
            <HeaderView
                title={CommonString.eVsNearYou}
            />

            <FlatList
                data={dummyData}
                showsVerticalScrollIndicator={false}
                renderItem={renderView}
                contentContainerStyle={{paddingBottom:20,marginHorizontal:20}}
                ItemSeparatorComponent={renderSeparate}
            />
        </SafeAreaView>
    )
}
export default BikesScreen;