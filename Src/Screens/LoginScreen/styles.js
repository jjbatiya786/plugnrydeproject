import { StyleSheet } from 'react-native';
import { colors } from '../../Resource/Colors';
import { isDarkTheme } from '../../Utilities/helper';
import { Fonts } from '../../Resource/Fonts';

export default styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
    },
    imgLogo: {
        width: 46,
        height: 31,
    },
    headerView: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'space-between',
        marginTop: 35
    },
    txtTitle: {
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 30,
        lineHeight: 30,
        fontWeight: "500",
        color: isDarkTheme ? colors.yellow : colors.black,
        marginTop: 50,
        marginBottom: 10
    },
    txtContent: {
        fontFamily:Fonts.ApexNew,
        fontSize: 18,
        lineHeight: 25,
        fontWeight: "500",
        color: isDarkTheme ? colors.white : colors.black,
        letterSpacing: 0.2,
    },
    textInputView: {
        flexDirection: "row",
        alignItems: "center",
        borderRadius: isDarkTheme ? 29 : 1,
        backgroundColor: isDarkTheme ? colors.textInputBackGroundDark : colors.white,
        height: 60,
        borderWidth: isDarkTheme ? 0 : 1,
        marginTop: 30
    },
    txtCode: {
        fontFamily:Fonts.Arial,
        fontSize: 18,
        lineHeight: 25,
        fontWeight: "400",
        color: isDarkTheme ? colors.white : colors.black,
        letterSpacing: 0.2,
    },
    vertiCalLine: {
        width: 1,
        backgroundColor: isDarkTheme ? colors.borderColor : colors.black,
        height: 30,
        marginLeft: 10,
    },
    textInput: {
        fontFamily:Fonts.Arial,
        flex: 0.8,
        fontSize: 18,
        lineHeight: 25,
        fontWeight: "400",
        color: isDarkTheme ? colors.white : colors.black,
        letterSpacing: 0.2,
    },
    pinCodeView: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: 16
    },
    roundView: {
        width: 160,
        height: 60,
        borderRadius: 40,
        backgroundColor: colors.sendOtpBtnColor,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        position: "absolute",
        top: -60,
    },
    txtStyle: {
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: 'rgba(0, 0, 0, 0.4)',
        letterSpacing: 0.4,
    },
})