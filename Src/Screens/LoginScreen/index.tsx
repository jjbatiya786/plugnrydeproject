import React, { useEffect, useState } from 'react';
import { Image, ImageBackground, SafeAreaView, Text, TextInput, View, } from 'react-native';
import styles from './styles';
import { images } from '../../Resource/Images';
import { CommonString, NavStrings } from '../../Resource/Strings';
import { colors } from '../../Resource/Colors';
import { isDarkTheme } from '../../Utilities/helper';
import LinearGradient from 'react-native-linear-gradient';
import RoundButton from '../../Component/RoundButton';

const LoginScreen = (props:any) => {
    const [number, setNumber] = useState("")
    const onPress=()=>{
        if(number.length===10)
            props.navigation.navigate(NavStrings.verifyScreen);
    }
    return (
        <SafeAreaView style={styles.mainView}>
            <View style={{ paddingHorizontal: 20 }}>
                <View style={styles.headerView}>
                    <Image style={styles.imgLogo}
                        source={images.appIcon}
                        resizeMode={'contain'} />
                </View>
                <Text style={styles.txtTitle}>{CommonString.enterNumber}</Text>
                <Text style={styles.txtContent}>{CommonString.optDesc}</Text>
                <View style={styles.textInputView}>
                    <View style={styles.pinCodeView}>
                        <Text style={styles.txtCode}>{"+91"}</Text>
                        <View style={styles.vertiCalLine} />
                    </View>
                    <TextInput
                        style={styles.textInput}
                        placeholder={'XXXXXXXXXX'}
                        placeholderTextColor={colors.placeHolderTextColor}
                        keyboardType={'phone-pad'}
                        value={number}
                        maxLength={10}
                        onChangeText={(value) => setNumber(value)}
                    />

                </View>
            </View>

            <ImageBackground
                source={isDarkTheme ? images.mapImageDark : images.mapImageLight}
                style={{ flex: 1, marginTop: 100 }}
            >
                <LinearGradient
                    colors={isDarkTheme ? colors.darkShadowLoginScreen : colors.lightShadowLoginScreen}
                    style={{ flex: 1 }}
                />
                <RoundButton
                    style={number.trim().length != 10 ? styles.roundView : [styles.roundView, { backgroundColor: colors.yellow }]}
                    txt={CommonString.sendOtp}
                    txtStyle={number.trim().length != 10 ? styles.txtStyle : [styles.txtStyle, { color: colors.black }]}
                    onPress={onPress}
                />
            </ImageBackground>
        </SafeAreaView>
    )
}
export default LoginScreen;