import React, { useEffect } from 'react';
import { Image, SafeAreaView, } from 'react-native';
import { images } from '../../Resource/Images';
import styles from './styles';
import { NavStrings } from '../../Resource/Strings';

const SplashScreen = (props:any) => {
    useEffect(() => {
        setTimeout(() => {
            props.navigation.navigate(NavStrings.introScreen)
        }, 1000);
    }, [])
    return (
        <SafeAreaView style={styles.mainView}>
            <Image source={images.appIcon} style={styles.imgView} resizeMode={'contain'} />
        </SafeAreaView>
    )
}
export default SplashScreen;