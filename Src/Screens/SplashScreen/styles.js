import { StyleSheet } from 'react-native';
import { colors } from '../../Resource/Colors';

export default styles = StyleSheet.create({
    mainView:{
        flex:1,
        alignItems:"center",
        justifyContent:'center',
        backgroundColor:colors.backgroundColor
    },
    imgView:{
        height:116,
    }
})
