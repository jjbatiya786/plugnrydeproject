import React, { useEffect, useState } from 'react';
import { SafeAreaView, Text } from 'react-native';
import styles from './styles';
import BikeModal from '../../Component/BikeModal';
import { NavStrings } from '../../Resource/Strings';
const HomeScreen = (props: any) => {
    const [isOpen, setIsopen] = useState(true)
    const onCancelClick = () => {
        setIsopen(false)
    }
    const onSelectBike = () => {
        props.navigation.navigate(NavStrings.selctBikeView)
    }
    return (
        <SafeAreaView style={styles.mainView}>
            <BikeModal
                isOpen={isOpen}
                onCancelClick={onCancelClick}
                onSelectBike={onSelectBike}

            />
        </SafeAreaView>
    )
}
export default HomeScreen;