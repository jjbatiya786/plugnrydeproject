
import React, { useEffect, useState } from 'react';
import { FlatList, Image, ImageBackground, Platform, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import styles from './styles';
import HeaderBack from '../../Component/HeaderBack';
import { CommonString } from '../../Resource/Strings';
import { images } from '../../Resource/Images';
import { colors } from '../../Resource/Colors';

const walletHistory = [
    {
        id: 1,
        title: "CashBack",
        date: '6th July, 2023',
        transactionID: '234',
        isCredit: true,
        amount: '₹25'
    },
    {
        id: 2,
        title: "Paid to SSK Bike Point ",
        date: '6th July, 2023',
        transactionID: '235',
        isCredit: false,
        amount: '₹100'
    },
    {
        id: 1,
        title: "CashBack",
        date: '6th July, 2023',
        transactionID: '236',
        isCredit: true,
        amount: '₹25'
    },
    {
        id: 2,
        title: "Paid to SSK Bike Point ",
        date: '6th July, 2023',
        transactionID: '237',
        isCredit: false,
        amount: '₹100'
    }
]
const MyWalletScreen = (props: any) => {
    const onBackPress = () => {
        props.navigation.goBack(null)
    }
    const onLogoutClick = () => {

    }

    const cardView = () => {
        return (
            <ImageBackground
                style={styles.walletImageStyle}
                source={images.myWalletImg}
                resizeMode={Platform.OS == "android" ? "stretch" : "contain"}
            >
                <View style={{ flex: 1 }}>
                    <View style={styles.viewBalance}>
                        <Text style={styles.txtTotalBalance}>{CommonString.totalBalance}</Text>
                        <Text style={styles.txtTotalBalanceValue}>{"₹890"}</Text>
                    </View>
                    <View style={styles.horizontalLine} />
                    <View style={styles.bottomView}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image style={styles.cycleImgStyle} source={images.cycle} resizeMode={"contain"} />
                            <View>
                                <Text style={styles.txtLabel}>{CommonString.rydeBalance}</Text>
                                <Text style={styles.txtValue}>{"₹890"}</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image style={styles.rewardImgStyle} source={images.rewardImg} resizeMode={"contain"} />
                            <View>
                                <Text style={styles.txtLabel}>{CommonString.rewardBalance}</Text>
                                <Text style={styles.txtValue}>{"₹890"}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        )
    }
    const renderItem = ({ item, index }: any) => {
        return (
            <View style={styles.renderView}>
                <View style={{flex:0.8}}>
                    <Text style={styles.txtTitle} numberOfLines={1}>{item.title}</Text>
                    <View style={{ flexDirection: "row", marginTop: 7 }}>
                        <View>
                            <Text style={styles.txtLabel}>{CommonString.date}</Text>
                            <Text style={styles.txtValue}>{item.date}</Text>
                        </View>
                        <View style={{ marginLeft: 19 }}>
                            <Text style={styles.txtLabel}>{CommonString.transactionID}</Text>
                            <Text style={styles.txtValue}>{item.transactionID}</Text>
                        </View>
                    </View>
                </View>
                <View style={{flex:0.2}}>
                    <Text style={styles.txtLabel}>{item.isCredit ? CommonString.credit : CommonString.debit}</Text>
                    <Text style={[styles.txtAmount, { color: item.isCredit ? colors.green : colors.tomato }]}>{item.amount}</Text>
                </View>
            </View>
        )
    }
    const ItemSeparatorComponent = () => {
        return (
            <View style={styles.horizontalLine} />
        )
    }
    return (
        <ScrollView style={styles.mainView} >
            <SafeAreaView >
                <HeaderBack
                    title={CommonString.myWallet}
                    image={images.backImage}
                    onBackPress={onBackPress}
                    logoutTxt={CommonString.logoutTxt}
                    onLogoutClick={onLogoutClick}
                />
                {cardView()}
                <TouchableOpacity style={styles.addBalanceStyle}>
                    <Text style={styles.txtVaddBalance}>{CommonString.addBALANCE}</Text>
                </TouchableOpacity>
                <Text style={styles.txtWalletHistory}>{CommonString.walletHistory}</Text>
                <FlatList
                    data={walletHistory}
                    showsHorizontalScrollIndicator={false}
                    style={styles.flatList}
                    renderItem={renderItem}
                    ItemSeparatorComponent={ItemSeparatorComponent}
                />
            </SafeAreaView>
        </ScrollView>
    )
}
export default MyWalletScreen;
