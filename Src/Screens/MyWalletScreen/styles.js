import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors } from '../../Resource/Colors';
import { isDarkTheme } from '../../Utilities/helper';
import { Fonts } from '../../Resource/Fonts';
const { height, width } = Dimensions.get("screen")

export default styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
    },
    walletImageStyle: {
        height: 200,
        marginHorizontal: 20,
        shadowColor: colors.yellow,
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.5,
        shadowRadius: 10,
        elevation: 15,
        backgroundColor: Platform.OS == "android" ? colors.backgroundColor : undefined,
        borderRadius: 12,
        marginVertical: 20,

    },
    txtTotalBalance: {
        fontFamily: Fonts.ApexNew,
        fontWeight: "500",
        fontSize: 18,
        lineHeight: 19,
        letterSpacing: 0.2,
        color: colors.white
    },
    txtTotalBalanceValue: {
        fontFamily: Fonts.Arial,
        fontWeight: "700",
        fontSize: 40,
        letterSpacing: 0.2,
        color: colors.white
    },
    horizontalLine: {
        height: 1,
        backgroundColor: colors.white,
        opacity: 0.15,
        marginVertical:15,
    },
    viewBalance: { alignSelf: 'center', marginTop: 10, alignItems: "center", justifyContent: "center",marginTop:24 },
    bottomView:{
        alignItems:"center",
        flexDirection:'row',
        marginHorizontal:20,
        justifyContent:"space-between"
    },
    cycleImgStyle:{
        width:23.08,
        height:20,
        tintColor:colors.white,
        marginRight:15
    },
    txtLabel:{
        fontFamily: Fonts.Arial,
        fontWeight: "400",
        fontSize: 14,
        letterSpacing: 0.2,
        lineHeight:19,
        color: colors.white,
        opacity:0.6
    },
    txtValue:{
        fontFamily: Fonts.Arial,
        fontWeight: "700",
        fontSize: 14,
        letterSpacing: 0.2,
        lineHeight:19,
        color: colors.white,
    },
    rewardImgStyle:{
        width:20,
        height:20,
        tintColor:colors.white,
        marginRight:15
    },
    addBalanceStyle:{
        height:60,
        alignSelf:'center',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:40,
        borderWidth:1,
        borderColor:colors.yellow,
        paddingHorizontal:40

    },
    txtVaddBalance:{
        fontFamily: Fonts.ApexNew,
        fontWeight: "600",
        fontSize: 16,
        letterSpacing: 0.4,
        lineHeight:25,
        color: colors.white,
    },
    txtWalletHistory:{
        fontFamily: Fonts.ApexNewMedium,
        fontWeight: "600",
        fontSize: 16,
        lineHeight:25,
        color: colors.white,
        marginHorizontal:20,
        marginTop:30,
    },
    flatList:{
        borderWidth:1,
        borderColor:colors.modelBackGroundColor,
        borderRadius:25,
        marginHorizontal:20,
        paddingVertical:20,
        marginTop:10,
        marginBottom:20
    },
    txtTitle:{
        fontFamily: Fonts.Arial,
        fontWeight: "700",
        fontSize: 18,
        letterSpacing: 0.2,
        lineHeight:25,
        color: colors.white,
    },
    txtAmount:{
        fontFamily: Fonts.Arial,
        fontWeight: "700",
        fontSize: 24,
        letterSpacing: 0.2,
        lineHeight:25,
    },
    renderView:{ flexDirection: 'row', marginHorizontal: 20, alignItems: 'center', justifyContent: "space-between" }

})