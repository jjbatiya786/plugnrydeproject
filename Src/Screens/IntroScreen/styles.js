import { StyleSheet } from 'react-native';
import { colors } from '../../Resource/Colors';
import { isDarkTheme } from '../../Utilities/helper';
import { Fonts } from '../../Resource/Fonts';

export default styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
    },
    imgLogo: {
        width: 46,
        height: 31,
    },
    headerView: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'space-between',
        marginTop: 35
    },
    txtSkip: {
        fontFamily:Fonts.ApexNewMedium,
        fontWeight: "500",
        fontSize: 16,
        lineHeight: 25,
        letterSpacing: 0.4,
        color: isDarkTheme ? colors.yellow : colors.black,
    },
    txtTitle: {
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 30,
        lineHeight: 31,
        fontWeight: "500",
        color: isDarkTheme ? colors.yellow : colors.black,
        marginTop: 50,
        marginBottom:10
    },
    txtContent: {
        fontFamily:Fonts.ApexNew,
        fontSize: 18,
        lineHeight: 25,
        fontWeight: "500",
        color: isDarkTheme ? colors.white : colors.black,
        letterSpacing: 0.2,
    },
    imgCycle: {
        flex: 1,
    },
    txtStep: {
        fontFamily:Fonts.Arial,
        fontSize: 18,
        lineHeight: 25,
        letterSpacing: 0.2,
        color: colors.yellow,
        fontWeight: "400"
    },
    roundView: {
        height: 80,
        width: 80,
        borderRadius: 40,
        position: "absolute",
        bottom: 20,
        backgroundColor: colors.yellow,
        alignItems: "center",
        justifyContent: "center",
        right: 20,
    },
    roundViewLeft: {
        height: 80,
        width: 80,
        borderRadius: 40,
        position: "absolute",
        bottom: 20,
        backgroundColor: colors.white,
        alignItems: "center",
        justifyContent: "center",
        left: 20,
    },
    btnimgView: {
        width: 20.41,
        height: 16.71,
    },
    roundViewLastStep: {
        height: 80,
        width: 160,
        borderRadius: 40,
        position: "absolute",
        bottom: 20,
        backgroundColor: colors.yellow,
        alignItems: "center",
        justifyContent: "center",
        right: 20,
    },
    txtStyle:{
        fontFamily:Fonts.ApexNewMedium,
        color:colors.black,
        fontSize:16,
        lineHeight:25,
        letterSpacing:0.4,
        fontWeight:"500",
    }
})
