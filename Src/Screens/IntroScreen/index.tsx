import React, { useEffect, useState } from 'react';
import { Alert, Image, ImageBackground, SafeAreaView, Text, Vibration, View, } from 'react-native';
import styles from './styles';
import { images } from '../../Resource/Images';
import { CommonString, NavStrings } from '../../Resource/Strings';
import { isDarkTheme } from '../../Utilities/helper';
import LinearGradient from 'react-native-linear-gradient';
import { colors } from '../../Resource/Colors';
import RoundButton from '../../Component/RoundButton';
const totalStep = 2;
const IntroScreen = (props: any) => {
    const [step, setStep] = useState(1);
    const [stepTitle, setStepTitle] = useState("")
    const [stepContent, setStepContent] = useState("")
    const [imgSource, setImgSource] = useState(null)
    useEffect(() => {
        switch (step) {
            case 1:
                setStepTitle(CommonString.step1Title);
                setStepContent(CommonString.step1Content);
                setImgSource(images.imgCycleLight1);
                break;
            case 2:
                setStepTitle(CommonString.step2Title);
                setStepContent(CommonString.step2Content)
                setImgSource(images.imgCycleLight2);
                break;
            default:
                break;
        }
    }, [step])
    const onPress = () => {
        setStep(step + 1)
    }
    const onPressLeft = () => {
        setStep(step - 1)
    }
    const onPressLastStep = () => {
        props.navigation.navigate(NavStrings.loginScreen)
    }
    const onClickSkip=()=>{
        props.navigation.replace(NavStrings.tabNavigation)

    }
    return (
        <SafeAreaView style={styles.mainView}>
            <View style={{ flex: 0.4, paddingHorizontal: 20 }}>
                <View style={styles.headerView}>
                    <Image style={styles.imgLogo}
                        source={images.appIcon}
                        resizeMode={'contain'} />
                    {step != totalStep && <Text onPress={onClickSkip} style={styles.txtSkip}>{CommonString.skip}</Text>}
                </View>
                <Text style={styles.txtTitle}>{stepTitle}</Text>
                <Text style={styles.txtContent}>{stepContent}</Text>
            </View>
            <LinearGradient style={{ flex: 0.6, paddingHorizontal: 20 }}
                start={{ x: 0.5, y: 0.5 }} end={{ x: 0.5, y: 1 }}
                colors={[colors.backgroundColor, colors.colorGradientThemeWise]}
            >
                {imgSource != null &&
                    <ImageBackground
                        style={styles.imgCycle}
                        source={imgSource}
                        resizeMode={"contain"}>
                        <Text style={styles.txtStep}>{step + "/" + totalStep}</Text>
                    </ImageBackground>}
            </LinearGradient>

            {step == 1 && <RoundButton
                style={styles.roundView}
                btnImgStyle={styles.btnimgView}
                onPress={onPress}
                image={images.rightArrow}
            />}
            {step > 1 && <RoundButton
                style={styles.roundViewLeft}
                btnImgStyle={styles.btnimgView}
                onPress={onPressLeft}
                image={images.leftArrow}
            />}
            {step === totalStep && <RoundButton
                style={styles.roundViewLastStep}
                onPress={onPressLastStep}
                txtStyle={styles.txtStyle}
                txt={CommonString.bookRyde}
            />}
        </SafeAreaView>
    )
}
export default IntroScreen;