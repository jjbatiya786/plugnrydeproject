import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors } from '../../Resource/Colors';
import { isDarkTheme } from '../../Utilities/helper';
import { Fonts } from '../../Resource/Fonts';
const { height, width } = Dimensions.get("screen")

export default styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
    },
    roundView: {
        height: 140,
        width: 140,
        borderRadius: 70,
        backgroundColor: colors.textInputBackGroundDark,
        alignSelf: 'center',
        marginVertical: 24,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgProfileView: {
        width: 59,
        height: 66,
    },
    plusView: {
        height: 30,
        width: 30,
        borderRadius: 15,
        backgroundColor: colors.green,
        position: 'absolute',
        right: 0,
        alignItems: 'center',
        justifyContent: 'center',
        top: 40
    },
    imgPlus: {
        height: 14,
        width: 14,
    },
    cornerView: {
        borderWidth: 1,
        borderColor: isDarkTheme ? colors.borderColorLineDark : colors.blue,
        borderRadius: 25,
        margin: 20
    },
    txtLabel: {
        fontFamily: Fonts.Arial,
        fontWeight: "400",
        fontSize: 14,
        lineHeight: 19,
        letterSpacing: 0.2,
        color: isDarkTheme ? colors.white : colors.black,
    },
    txtValue: {
        fontFamily: Fonts.Arial,
        fontWeight: "700",
        fontSize: 18,
        lineHeight: 25,
        letterSpacing: 0.2,
        color: isDarkTheme ? colors.white : colors.black
    },
    editImg: {
        width: 16,
        height: 19,
        tintColor: isDarkTheme ? colors.white : colors.black
    },
    horizontalLine: {
        height: 1,
        backgroundColor: isDarkTheme ? colors.horizontalLineDarkColor : colors.horizontalLineLightColor,
    },
    viewWallet: {
        flexDirection: 'row',
        backgroundColor: colors.yellow,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: 25,
        marginHorizontal: 20,
        padding: 20

    },
    txtWallet: {
        fontWeight: '400',
        fontSize: 14,
        lineHeight: 19,
        letterSpacing: 0.2,
        color: isDarkTheme ? colors.white : colors.black,
    },
    txtWalletValue: {
        fontFamily: Fonts.Arial,
        fontWeight: '700',
        fontSize: 18,
        lineHeight: 25,
        letterSpacing: 0.2,
        color: colors.black
    },
    paymentHistoryView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: 25,
        marginHorizontal: 20,
        padding: 20,
        borderWidth: 1,
        borderColor: isDarkTheme ? colors.borderColorLineDark : colors.blue,
        marginVertical: 20

    },
    arrowStyle: {
        width: 15,
        height: 12,
        tintColor: isDarkTheme ? colors.white : colors.black,
        transform: [{ rotate: '180deg' }]
    },
    walletImageStyle: {
        height: 200,
        marginHorizontal: 20,
        justifyContent: 'flex-end',
        shadowColor: colors.yellow,
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.5,
        shadowRadius: 10,
        elevation: 15,
        backgroundColor: Platform.OS == "android" ? colors.backgroundColor:undefined,
        borderRadius:12

    },
    txtWalletBalance: {
        fontFamily: Fonts.Arial,
        fontWeight: '700',
        fontSize: 28,
        lineHeight: 30,
        letterSpacing: 0.2,
        color: isDarkTheme ? colors.white : colors.black
    }

});