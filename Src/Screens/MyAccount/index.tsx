import React, { useEffect, useState } from 'react';
import { Image, ImageBackground, Platform, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import styles from './styles';
import { CommonString, NavStrings } from '../../Resource/Strings';
import { images } from '../../Resource/Images';
import HeaderBack from '../../Component/HeaderBack';
import { isDarkTheme } from '../../Utilities/helper';

const MyAccount = (props:any) => {
    const onBackPress = () => {

    }
    const onLogoutClick = () => {

    }
    const viewLabel = (label: any, value: any) => {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: "space-between", marginVertical: 10, marginHorizontal: 20 }}>
                <View>
                    <Text style={styles.txtLabel}>{label}</Text>
                    <Text style={styles.txtValue}>{value}</Text>
                </View>
                <TouchableOpacity>
                    <Image style={styles.editImg} source={images.editImg} resizeMode={"contain"} />
                </TouchableOpacity>
            </View>
        )
    }
    return (
        <ScrollView style={styles.mainView} >
            <SafeAreaView >
                <HeaderBack
                    title={CommonString.myAccount}
                    // image={images.backImage}
                    // onBackPress={onBackPress}
                    logoutTxt={CommonString.logoutTxt}
                    onLogoutClick={onLogoutClick}
                />
                <View style={{ alignSelf: 'center' }}>
                    <View style={styles.roundView}>
                        <Image style={styles.imgProfileView} source={images.imgProfile} resizeMode={"contain"} />
                    </View>
                    <TouchableOpacity style={styles.plusView}>
                        <Image style={styles.imgPlus} source={images.plusIcon} resizeMode={"contain"} />
                    </TouchableOpacity>
                </View>
                <View style={styles.cornerView}>
                    {viewLabel(CommonString.name, "Vishal Gupta")}
                    <View style={styles.horizontalLine} />
                    {viewLabel(CommonString.phone, "+91 9876543210")}
                    <View style={styles.horizontalLine} />
                    {viewLabel(CommonString.phone, "vishal.gupta@gmail.com")}
                </View>
                {/* {isDarkTheme == false ? <View style={styles.viewWallet}>
                    <Text style={styles.txtWallet}>{CommonString.walletBalance}</Text>
                    <Text style={styles.txtWalletValue}>{"₹890"}</Text>
                </View> : */}

                <ImageBackground
                    style={styles.walletImageStyle}
                    source={images.walletImage}
                    resizeMode={Platform.OS=="android"? "stretch":"contain"}
                >
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 20, marginHorizontal: 30, }}>
                        <View>
                            <Text style={[styles.txtLabel, { opacity: 0.6 }]}>{CommonString.name}</Text>
                            <Text style={[styles.txtValue, { opacity: 0.6 }]}>{'Vishal Gupta'}</Text>
                        </View>
                        <TouchableOpacity onPress={()=>props.navigation.navigate(NavStrings.myWalletScreen)}>
                            <Text style={[styles.txtWallet, { marginBottom: 10 }]}>{CommonString.walletBalance}</Text>
                            <Text style={styles.txtWalletBalance}>{'₹890'}</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
                {/* } */}
                <TouchableOpacity style={styles.paymentHistoryView}>
                    <Text style={[styles.txtWallet]}>{CommonString.paymentHistory}</Text>
                    <Image style={styles.arrowStyle} source={images.backImage} resizeMode={"contain"} />
                </TouchableOpacity>
            </SafeAreaView>
        </ScrollView>
    )
}
export default MyAccount;