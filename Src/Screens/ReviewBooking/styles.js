import { StyleSheet, Dimensions } from 'react-native';
import { colors } from '../../Resource/Colors';
import { isDarkTheme } from '../../Utilities/helper';
import { Fonts } from '../../Resource/Fonts';
const { height, width } = Dimensions.get("screen")

export default styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
    },
    roundView:{
        height:260,
        width:260,
        borderRadius:130,
        backgroundColor:colors.textInputBackGroundDark,
        alignSelf:'center',
        marginVertical:33
    },
    cycleView:{
        width:width,
        height:310,
        position:'absolute',
        top:10
    },
    detailView:{
        borderWidth:1,
        borderColor:colors.modelBackGroundColor,
        borderRadius:25,
        margin:20,
        paddingVertical:20

    },
    modelView:{
        alignItems:'center',
        justifyContent:'space-between',
        flexDirection:"row",
        marginHorizontal:20
    },
    txtLabel:{
        fontFamily:Fonts.Arial,
        fontWeight:'400',
        fontSize:14,
       // lineHeight:19,
        letterSpacing:0.2,
        color:colors.white
    },
    txtModal:{
        fontFamily:Fonts.Arial,
        fontWeight:'700',
        fontSize:18,
        lineHeight:25,
        letterSpacing:0.2,
        color:colors.white
    },
    viewHorizonatlLine:{
        height:1,
        backgroundColor:colors.white,
        opacity:0.15,
        marginVertical:20
    },
    durationView:{
        flex:0.4
    },
    txtPlace:{
        fontFamily:Fonts.ApexNewMedium,
        fontSize:18,
        fontWeight:"600",
        color:isDarkTheme?colors.white:colors.black,
        lineHeight:25,
        letterSpacing:0.2,
    
    },
    txtAddress:{
        fontFamily:Fonts.ApexNew,
        fontSize:16,
        fontWeight:"500",
        color:isDarkTheme?colors.white:colors.black,
        lineHeight:25,
        letterSpacing:0.2,
    },
    imgPlace:{
        width:14,
        height:20,
        tintColor:isDarkTheme?colors.green:undefined,
    },
    txtNearTag:{
        fontFamily:Fonts.Arial,
        fontSize:14,
        fontWeight:"400",
        color:isDarkTheme?colors.white:colors.black,
        lineHeight:25,
        letterSpacing:0.2,
    },
    placeView:{
         flex: 0.2, alignItems: 'center', justifyContent: 'flex-start' ,marginTop:5
    },
    headerView:{flexDirection:'row',alignItems:'center',justifyContent:"space-between",marginVertical:10},
    txtRental:{
        fontFamily:Fonts.Arial,
        fontSize:16,
        fontWeight:"700",
        color:isDarkTheme?colors.white:colors.black,
        lineHeight:25,
        letterSpacing:0.2,
    },
    rentalView:{
        borderWidth:1,
        borderColor:colors.modelBackGroundColor,
        borderRadius:25,
        margin:20,
        paddingTop:20,
        flex:1
        
    },
    grandTotalView:{
        alignItems:'center',
        justifyContent:'space-between', 
        flexDirection:'row',
        backgroundColor:colors.yellow,
        paddingHorizontal:20,
        marginTop:20,
        borderBottomLeftRadius:25,
        borderBottomRightRadius:25,
        paddingVertical:20
    },
    roundViewBtn: {
        height: 60,
        borderRadius: 40,
        backgroundColor: colors.green,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        width:'100%',

    },
    txtStyle: {
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: colors.white,
        letterSpacing: 0.4,
    },
    txtCancel:{
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: colors.white,
        letterSpacing: 0.2,
        opacity:0.5,
        textAlign:"center",
        marginVertical:20
    },
    viewHorizonatlLineNew:{
        width:width,
        height:2,
        backgroundColor:colors.horizontalLine,
        marginBottom:20
    }
      
})