import React, { useEffect, useState } from 'react';
import { Dimensions, Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import styles from './styles';
import HeaderBack from '../../Component/HeaderBack';
import { CommonString, NavStrings } from '../../Resource/Strings';
import { images } from '../../Resource/Images';
import { colors } from '../../Resource/Colors';
import RoundButton from '../../Component/RoundButton';

const ReviewBooking = (props: any) => {
    const onBackPress = () => {
        props.navigation.goBack(null);
    }
    const onPress=()=>{
        props.navigation.navigate(NavStrings.bookingConfirmedScreen)
    }
    return (
        <ScrollView style={styles.mainView} showsVerticalScrollIndicator={false} >
            <SafeAreaView >
                <HeaderBack
                    title={CommonString.reviewBooking}
                    image={images.backImage}
                    onBackPress={onBackPress}
                    marginRight={30}
                />
                <View>
                    <View style={styles.roundView} />
                    <Image style={styles.cycleView} source={images.bikes} resizeMode={"stretch"} />
                </View>
                <View style={styles.detailView}>
                    <View style={styles.modelView}>
                        <Text style={styles.txtLabel}>{CommonString.model}</Text>
                        <Text style={styles.txtModal}>{"EPICK - N150"}</Text>
                    </View>
                    <View style={styles.viewHorizonatlLine} />
                    <View style={{ flexDirection: 'row', marginHorizontal: 20 }}>
                        <View style={styles.durationView}>
                            <Text style={styles.txtLabel}>{CommonString.duration}</Text>
                            <Text style={styles.txtModal}>{'4 hrs'}</Text>
                        </View>
                        <View style={{ flex: 0.3 }}>
                            <Text style={styles.txtLabel}>{CommonString.startTime}</Text>
                            <Text style={[styles.txtLabel, { lineHeight: 20 }]}>{"6th July,2023 05:00 PM"}</Text>
                        </View>
                        <View style={{ flex: 0.3, marginLeft: 10 }}>
                            <Text style={styles.txtLabel}>{CommonString.endTime}</Text>
                            <Text style={[styles.txtLabel, { lineHeight: 20 }]}>{"6th July,2023 09:00PM"}</Text>
                        </View>
                    </View>
                    <View style={styles.viewHorizonatlLine} />
                    <View style={{ marginHorizontal: 20 }}>
                        <Text style={styles.txtLabel}>{CommonString.pickupLocation}</Text>
                        <View style={styles.headerView}>
                            <View style={{ flex: 0.8 }}>
                                <Text style={styles.txtPlace}>{"Casbah"}</Text>
                                <Text style={styles.txtAddress} numberOfLines={1}>{"Next to Westin Hotel, HITEC Next to Westin Hotel, HITEC..."}</Text>
                            </View>
                            <View style={styles.placeView}>
                                <Image style={styles.imgPlace} source={images.placeIcon} />
                                <Text style={styles.txtNearTag}>{"800 m"}</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={[styles.rentalView]}>
                    <View style={styles.modelView}>
                        <Text style={styles.txtLabel}>{CommonString.rentalFare}</Text>
                        <Text style={styles.txtRental}>{"₹120"}</Text>
                    </View>
                    <View style={[styles.modelView,{marginTop:10}]}>
                        <Text style={styles.txtLabel}>{CommonString.initialDeposit}</Text>
                        <Text style={styles.txtRental}>{"₹200"}</Text>
                    </View>
                    <View style={styles.grandTotalView}>
                        <Text style={[styles.txtLabel,{color:colors.black}]}>{CommonString.totalPayAmt}</Text>
                        <Text style={[styles.txtRental,{color:colors.black}]}>{"₹320"}</Text>
                    </View>
                </View>
                <View style={styles.viewHorizonatlLineNew} />
                <View style={{ marginHorizontal: 20 }}>
                    <RoundButton
                        style={styles.roundViewBtn}
                        txt={"PAY NOW  | ₹320"}
                        txtStyle={styles.txtStyle}
                        onPress={onPress}
                    />
                    <Text onPress={onBackPress} style={styles.txtCancel}>{CommonString.cancel}</Text>
                </View>
            </SafeAreaView>
        </ScrollView>
    )
}
export default ReviewBooking;