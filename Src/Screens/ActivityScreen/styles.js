import { StyleSheet, Dimensions } from 'react-native';
import { colors } from '../../Resource/Colors';
import { isDarkTheme } from '../../Utilities/helper';
import { Fonts } from '../../Resource/Fonts';
const { height, width } = Dimensions.get("screen")

export default styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
    },

    //renderview
    horizontalLine: {
        marginVertical: 15
    },
    roundView1: {
        height: 80, width: 80, borderRadius: 85,
         backgroundColor:isDarkTheme? colors.yellow:colors.lightThemeBlue,
        marginLeft: 18


    },
    renderView: {
        borderWidth: 1,
        borderColor: isDarkTheme ? colors.white : colors.lightBlue,
        borderRadius: width / 2,
        flexDirection: 'row'

    },
    imgBikes: {
        height: 70,
        width: 90,
        position: 'absolute',
        left: 8

    },
    bikesView: {
        marginRight: 10,
        marginVertical: 18,
    },
    txtPlace: {
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 18,
        fontWeight: "600",
        color: isDarkTheme ? colors.white : colors.black,
        lineHeight: 25,
        letterSpacing: 0.2,

    },
    txtAddress: {
        fontFamily:Fonts.Arial,
        fontSize: 14,
        fontWeight: "400",
        color: isDarkTheme ? colors.white : colors.black,
        lineHeight: 19,
        letterSpacing: 0.2,
    },
    txtCharge: {
        fontFamily:Fonts.Arial,
        fontSize: 18,
        fontWeight: "700",
        color: isDarkTheme ? colors.yellow : colors.black,
        lineHeight: 25,
        letterSpacing: 0.2,
        marginVertical: 18,
        flex: 0.3,
        alignSelf: 'center',
        bottom: 5
    }
});