import React, { useEffect, useState } from 'react';
import { FlatList, Image, SafeAreaView, Text, View } from 'react-native';
import styles from './styles';
import HeaderView from '../../Component/HeaderView';
import { CommonString } from '../../Resource/Strings';
import { images } from '../../Resource/Images';
const dummyData = [
    {
        content: 'Distance 10km',
        startDate: '6th July,2023',
        endDate: '8th July,2023',
        charge: '₹1200'
    },
    {
        content: 'Distance 10km',
        startDate: '6th July,2023',
        endDate: '8th July,2023',
        charge: '₹1200'
    },
    {
        content: 'Distance 10km',
        startDate: '6th July,2023',
        endDate: '8th July,2023',
        charge: '₹1200'
    },
    {
        content: 'Distance 10km',
        startDate: '6th July,2023',
        endDate: '8th July,2023',
        charge: '₹1200'
    },
    {
        content: 'Distance 10km',
        startDate: '6th July,2023',
        endDate: '8th July,2023',
        charge: '₹1200'
    },
    {
        content: 'Distance 10km',
        startDate: '6th July,2023',
        endDate: '8th July,2023',
        charge: '₹1200'
    },
    {
        content: 'Distance 10km',
        startDate: '6th July,2023',
        endDate: '8th July,2023',
        charge: '₹1200'
    },
    
    
]
const ActivityScreen = () => {
    const renderView = ({ item, index }: any) => {
        return (
            <View style={styles.renderView}>
                <View style={styles.bikesView}>
                    <View style={styles.roundView1} />
                    <Image style={styles.imgBikes} source={images.bikes} resizeMode={'contain'} />
                </View>
                <View style={{ flex: 0.7, marginVertical: 18 }}>
                    <Text style={styles.txtPlace}>{item.content}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.txtAddress}>{"Date : "}</Text>
                        <View>
                            <Text style={styles.txtAddress}>{item.startDate + " to\n" + item.endDate}</Text>
                        </View>
                    </View>
                </View>
                <Text style={styles.txtCharge}>{item.charge}</Text>
            </View>
        )
    }
    const renderSeparate = () => {
        return (
            <View style={styles.horizontalLine} />
        )
    }
    return (
        <SafeAreaView style={styles.mainView}>
            <HeaderView
                title={CommonString.activity}
            />

            <FlatList
                data={dummyData}
                showsVerticalScrollIndicator={false}
                renderItem={renderView}
                contentContainerStyle={{ paddingBottom: 20, marginHorizontal: 20 }}
                ItemSeparatorComponent={renderSeparate}
            />
        </SafeAreaView>
    )
}
export default ActivityScreen;