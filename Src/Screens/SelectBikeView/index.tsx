import React, { useEffect, useState } from 'react';
import { Dimensions, Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import styles from './styles';
import HeaderBack from '../../Component/HeaderBack';
import { CommonString, NavStrings } from '../../Resource/Strings';
import { images } from '../../Resource/Images';
import RoundButton from '../../Component/RoundButton';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { colors } from '../../Resource/Colors';
const dummyData = [
    {
        id: 1,
        modal: 'EPICK - N150',
        max_speed: '4 hrs',
        loadCapacity: '140kgs',
        batteryCapacity: '250w',
        range: '210 km',
        image: images.fullCycle
    },
    {
        id: 2,
        modal: 'EPICK - N150',
        max_speed: '4 hrs',
        loadCapacity: '140kgs',
        batteryCapacity: '250w',
        range: '210 km',
        image: images.fullCycle
    },
    {
        id: 3,
        modal: 'EPICK - N150',
        max_speed: '4 hrs',
        loadCapacity: '140kgs',
        batteryCapacity: '250w',
        range: '210 km',
        image: images.fullCycle
    }
]

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.88);

const SelectBikeView = (props: any) => {
    const [activeSlide, setActiveSlide] = useState(0)
    const onBackPress = () => {
        props.navigation.goBack(null);
    }
    const onPress = () => {
        props.navigation.navigate(NavStrings.reviewBooking);

    }
    const descView = (label: any, value: any) => {
        return (
            <View style={{ flex: 0.5 }}>
                <Text style={styles.txtLabel}>{label}</Text>
                <Text style={styles.txtValue}>{value}</Text>
            </View>

        )
    }
    const _renderItem = ({ item, index }: any) => {
        return (
            <View style={styles.selectModelView}>
                <Image style={styles.subtractStyle} source={images.Subtract} resizeMode={"contain"} />
                <Image style={styles.fullImgCycle} source={images.fullCycle} resizeMode={"contain"} />
                <Text style={styles.txtModalName}>{item.modal}</Text>

                <View style={styles.descView}>
                    {descView(CommonString.maxSpeed, item.max_speed)}
                    {descView(CommonString.loadCapacity, item.loadCapacity)}
                </View>
                <View style={styles.descView}>
                    {descView(CommonString.batteryCapacity, item.batteryCapacity)}
                    {descView(CommonString.range, item.range)}
                </View>
                <TouchableOpacity style={styles.btnSelectStyle}>
                    <Text style={styles.txtSelect}>{CommonString.SELECT}</Text>
                </TouchableOpacity>
            </View>
        )
    }
    const pagination = () => {
        return (
            <Pagination
                dotsLength={dummyData.length}
                activeDotIndex={activeSlide}
                dotStyle={{
                    width: 12,
                    height: 12,
                    backgroundColor: colors.yellow,
                    borderRadius: 7.5,

                }}
                inactiveDotStyle={{
                    backgroundColor: colors.textInputBackGroundDark,
                    width: 20,
                    height: 20,
                    borderRadius: 10,
                }}

            />
        );
    }
    return (
        <ScrollView style={styles.mainView} showsVerticalScrollIndicator={false} >
            <SafeAreaView >
                <HeaderBack
                    title={CommonString.selectBikeSelectView}
                    image={images.backImage}
                    onBackPress={onBackPress}
                    marginRight={30}

                />
                <View style={styles.headerView}>
                    <View style={{ flex: 0.8 }}>
                        <Text style={styles.txtPlace}>{"Casbah"}</Text>
                        <Text style={styles.txtAddress} numberOfLines={1}>{"Next to Westin Hotel, HITEC Next to Westin Hotel, HITEC..."}</Text>
                    </View>
                    <View style={styles.placeView}>
                        <Image style={styles.imgPlace} source={images.placeIcon} />
                        <Text style={styles.txtNearTag}>{"800 m"}</Text>
                    </View>
                </View>
                <Carousel
                    data={dummyData}
                    renderItem={_renderItem}
                    sliderWidth={SLIDER_WIDTH}
                    itemWidth={ITEM_WIDTH}
                    onSnapToItem={(index) => setActiveSlide(index)}

                />
                {pagination()}
                <View style={styles.viewHorizonatlLine} />
                <View style={{ marginHorizontal: 20 }}>
                    <RoundButton
                        style={styles.roundView}
                        txt={CommonString.reviewBooking}
                        txtStyle={styles.txtStyle}
                        onPress={onPress}
                    />
                    <Text onPress={onBackPress} style={styles.txtCancel}>{CommonString.cancel}</Text>
                </View>
            </SafeAreaView>
        </ScrollView>
    )
}
export default SelectBikeView;