import { StyleSheet, Dimensions } from 'react-native';
import { colors } from '../../Resource/Colors';
import { isDarkTheme } from '../../Utilities/helper';
import { Fonts } from '../../Resource/Fonts';
const { height, width } = Dimensions.get("screen")

export default styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
    },
    txtPlace:{
        fontFamily:Fonts.ApexNewMedium,
        fontSize:18,
        fontWeight:"600",
        color:isDarkTheme?colors.white:colors.black,
        lineHeight:25,
        letterSpacing:0.2,
    
    },
    txtAddress:{
        fontFamily:Fonts.ApexNew,
        fontSize:16,
        fontWeight:"500",
        color:isDarkTheme?colors.white:colors.black,
        lineHeight:25,
        letterSpacing:0.2,
    },
    imgPlace:{
        width:14,
        height:20,
        tintColor:isDarkTheme?colors.green:undefined,
    },
    txtNearTag:{
        fontFamily:Fonts.Arial,
        fontSize:14,
        fontWeight:"400",
        color:isDarkTheme?colors.white:colors.black,
        lineHeight:25,
        letterSpacing:0.2,
    },
    placeView:{
         flex: 0.2, alignItems: 'center', justifyContent: 'flex-start' ,marginTop:5
    },
    headerView:{flexDirection:'row',alignItems:'center',justifyContent:"space-between",marginHorizontal:20,marginVertical:10},
    roundView: {
        height: 60,
        borderRadius: 40,
        backgroundColor: colors.yellow,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        width:'100%',

    },
    txtStyle: {
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: colors.black,
        letterSpacing: 0.4,
    },
    txtCancel:{
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: colors.white,
        letterSpacing: 0.2,
        opacity:0.5,
        textAlign:"center",
        marginVertical:20,
    },
    selectModelView:{
        backgroundColor:colors.textInputBackGroundDark,
        borderWidth:1,
        borderColor:colors.modelBackGroundColor,
        borderRadius:20,
    },
    subtractStyle:{
        width:260,
        height:160,
        alignSelf:'center'
    },
    fullImgCycle:{
        height:200,
        position:"absolute",
        width:'100%',
        top:15
    },
    txtModalName:{
        fontSize:30,
        color:colors.white,
        fontFamily:Fonts.ApexNew,
        letterSpacing:0.2,
        lineHeight:35,
        fontWeight:'700',
        marginHorizontal:20,
        top:20,
        marginBottom:20
    },
    descView:{
        flexDirection:'row',
        margin:20,
        alignItems:'flex-start',
     
    },
    txtLabel:{
        fontFamily:Fonts.Arial,
        color:colors.white,
        opacity:0.6,
        fontSize:14,
        fontWeight:'400',
        lineHeight:19,
        letterSpacing:0.2,
        
    },
    txtValue:{
        fontFamily:Fonts.Arial,
        color:colors.white,
        fontSize:18,
        fontWeight:'700',
        lineHeight:25,
        letterSpacing:0.2
    },
    btnSelectStyle:{
        alignItems:'center',
        justifyContent:'center',
        borderWidth:1,
        borderColor:colors.yellow,
        borderRadius:40,
        alignSelf:'center',
        width:160,
        height:60,
        marginVertical:20

    },
    txtSelect:{
        fontFamily:Fonts.ApexNew,
        color:colors.white,
        fontSize:16,
        fontWeight:'600',
        lineHeight:25,
        letterSpacing:0.4,
        textAlign:'center'
    },
    viewHorizonatlLine:{
        width:width,
        height:2,
        backgroundColor:colors.horizontalLine,
        marginBottom:20
    }
})