import React, { useEffect } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MyAccount from '../Screens/MyAccount';
import MyWalletScreen from '../Screens/MyWalletScreen';
import { CommonString, NavStrings } from '../Resource/Strings';
const Stack = createNativeStackNavigator();

const ProfileStack = ({ navigation, route }) => {

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
                headerBackTitleVisible: false
            }}>
            <Stack.Screen name={CommonString.myAccount} component={MyAccount} />
            <Stack.Screen name={NavStrings.myWalletScreen} component={MyWalletScreen} />
            
        </Stack.Navigator>
    )
}
export default ProfileStack;