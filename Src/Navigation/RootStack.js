import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavStrings } from '../Resource/Strings';
import AuthStackNavigation from './AuthStack';
import TabNavigation from './TabNavigation';
import SelectBikeView from '../Screens/SelectBikeView';
import ReviewBooking from '../Screens/ReviewBooking';
import BookingConfirmed from '../Screens/BookingConfirmed';
const RootStackNav = createNativeStackNavigator();

const RootStack = () => (
    <NavigationContainer >
        <RootStackNav.Navigator
            screenOptions={{
                headerShown: false, headerBackTitleVisible: false
            }}
        >
            <RootStackNav.Screen name={NavStrings.AuthStack} component={AuthStackNavigation} />
            <RootStackNav.Screen name={NavStrings.tabNavigation} component={TabNavigation} />
            <RootStackNav.Screen name={NavStrings.selctBikeView} component={SelectBikeView} />
            <RootStackNav.Screen name={NavStrings.reviewBooking} component={ReviewBooking} />
            <RootStackNav.Screen name={NavStrings.bookingConfirmedScreen} component={BookingConfirmed} />

        </RootStackNav.Navigator>
    </NavigationContainer>
)
export default RootStack;