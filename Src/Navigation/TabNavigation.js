import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Image, Text, View } from 'react-native';
import { images } from '../Resource/Images';
import HomeScreen from '../Screens/HomeScreen';
import { isDarkTheme } from '../Utilities/helper';
import { colors } from '../Resource/Colors';
import BikesScreen from '../Screens/BikesScreen';
import ActivityScreen from '../Screens/ActivityScreen';
import MyAccount from '../Screens/MyAccount';
import { CommonString } from '../Resource/Strings';
import { Fonts } from '../Resource/Fonts';
import ProfileStack from './ProfileStack';
const Tab = createBottomTabNavigator();

const TabNavigation = () => {
    const imageView = (image, color) => {
        return (
            <Image
                source={image}
                style={{
                    tintColor: color,
                    width: 20,
                    height: 20,
                }}
                resizeMode={"contain"}
            />
        )
    }
    return (
        <Tab.Navigator
            initialRouteName="Home"
            screenOptions={{
                tabBarActiveTintColor: isDarkTheme ? colors.yellow : colors.black,
                headerShown: false,
                tabBarStyle: { backgroundColor: isDarkTheme ? colors.textInputBackGroundDark : colors.white,paddingTop:10 },
                tabBarLabelStyle: {  lineHeight: 25,fontFamily:Fonts.ApexNewMedium, fontWeight: "600" }
            }}
        >
            <Tab.Screen
                name={CommonString.home}
                component={HomeScreen}
                options={{
                    tabBarLabel: CommonString.home,
                    tabBarIcon: ({ color, size }) => (
                        imageView(images.home, color)
                    ),
                }}
            />
            <Tab.Screen
                name={CommonString.bikes}
                component={BikesScreen}
                options={{
                    tabBarLabel: CommonString.bikes,
                    tabBarIcon: ({ color, size }) => (
                        imageView(images.cycle, color)
                    ),
                }}
            />
            <Tab.Screen
                name={CommonString.activity}
                component={ActivityScreen}
                options={{
                    tabBarLabel: CommonString.activity,
                    tabBarIcon: ({ color, size }) => (
                        imageView(images.activity, color)
                    ),
                }}
            />
            <Tab.Screen
                name={CommonString.account}
                component={ProfileStack}
                options={{
                    tabBarLabel: CommonString.account,
                    tabBarIcon: ({ color, size }) => (
                        imageView(images.myAccount, color)
                    ),
                }}
            />
        </Tab.Navigator>

    );
}
export default TabNavigation;