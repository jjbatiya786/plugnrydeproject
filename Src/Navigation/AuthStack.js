import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavStrings } from '../Resource/Strings';
import SplashScreen from '../Screens/SplashScreen';
import IntroScreen from '../Screens/IntroScreen';
import LoginScreen from '../Screens/LoginScreen';
import VerifyScreen from '../Screens/VerifyScreen';
const AuthStack = createNativeStackNavigator();
const AuthStackNavigation = () => {
    return (
        <AuthStack.Navigator
            screenOptions={{
                headerShown: false,
                headerBackTitleVisible: false
            }}>
            <AuthStack.Screen name={NavStrings.splashScreen} component={SplashScreen} />
            <AuthStack.Screen name={NavStrings.introScreen} component={IntroScreen} />
            <AuthStack.Screen name={NavStrings.loginScreen} component={LoginScreen} />
            <AuthStack.Screen name={NavStrings.verifyScreen} component={VerifyScreen} />

        </AuthStack.Navigator>
    )
}
export default AuthStackNavigation;