import { Appearance } from "react-native";
const isDarkTheme = true//Appearance.getColorScheme()==="dark"
export const images={
    appIcon:isDarkTheme? require("../Images/appIconDark.png"):require("../Images/appIconLight.png"),
    imgCycleDark1:require("../Images/imgCycleDark1.png"),
    imgCycleDark2:require("../Images/imgCycleDark2.png"),
    imgCycleLight2:require("../Images/imgCycleLight2.png"),
    imgCycleLight1:require("../Images/imgCycleLight1.png"),
    leftArrow:require("../Images/leftArrow.png"),
    rightArrow:require("../Images/rightArrow.png"),
    mapImageDark:require("../Images/mapImageDark.jpg"),
    mapImageLight:require("../Images/mapImageLight.jpg"),
    myAccount:require("../Images/MyAccount.png"),
    activity:require("../Images/Activity.png"),
    cycle:require("../Images/Cycle.png"),
    home:require("../Images/Home.png"),
    placeIcon:require("../Images/PlaceIcon.png"),    
    bikes:require("../Images/bikes.png"),    
    backImage:require("../Images/backImage.png"),
    imgProfile:require("../Images/imgProfile.png"),
    plusIcon:require("../Images/plusIcon.png"),
    editImg:require("../Images/editImg.png"),
    walletImage:require("../Images/walletImage.png"),
    ellipseImg:require("../Images/ellipseImg.png"),
    fullCycle:require("../Images/fullCycle.png"),
    Subtract:require("../Images/Subtract.png"),
    cycle2:require("../Images/cycle2.png"),
    myWalletImg:require("../Images/myWalletImage.png"),
    rewardImg:require("../Images/rewardImg.png"),


}
