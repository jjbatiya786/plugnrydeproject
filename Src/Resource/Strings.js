export const NavStrings = {
    AuthStack: "AuthStack",
    splashScreen: "SplashScreen",
    introScreen: "IntroScreen",
    loginScreen: 'LoginScreen',
    verifyScreen: 'VerifyScreen',
    tabNavigation: 'TabNavigation',
    selctBikeView: 'SelectBikeView',
    reviewBooking: 'ReviewBooking',
    bookingConfirmedScreen: 'BookingConfirmed',
    myWalletScreen:'MyWalletScreen'
}

export const CommonString = {
    //splash screen
    skip: 'SKIP',
    step1Title: 'Comprehensive Electric Mobility',
    step1Content: 'Our mobility services only involve Electric Vehicles that help solve the increase of harmful environmental gasses.',
    step2Content: 'Our technological solution involves easy and effective mobility services for your seamless commute.',
    step2Title: 'Keyless Mobility Technology',
    bookRyde: 'BOOK A RYDE',

    //Login Screen string
    enterNumber: "Enter Your Number",
    optDesc: "OTP will be sent to this number",
    sendOtp: "SEND OTP",

    //Verify Screen string
    verifyNumber: 'Verify Your Number',
    verifyDesc: 'OTP sent to ',
    verifyBtn: 'VERIFY',
    strChange: '(Change?)',
    resendOTP: "Resend OTP",

    //tab string
    account: 'Account',
    activity: 'Activity',
    bikes: 'Bikes',
    home: 'Home',

    //bikes screen string
    eVsNearYou: 'EVs Near You',

    //activity screen string
    //profile string
    myAccount: 'My Account',
    logoutTxt: 'Logout',
    name: 'Name',
    phone: 'Phone',
    email: 'Email',
    walletBalance: 'Wallet Balance',
    paymentHistory: 'Payment History',

    //model view string
    duration: 'Duration',
    strHour: 'Hour',
    strWeek: 'Week',
    strMonth: 'Month',
    selectNumberOf: 'Select number of hour(s)',
    selectBike: 'SELECT BIKE',
    cancel: 'Cancel',

    //select bike view
    selectBikeSelectView: 'Select Bike',
    reviewBooking: 'REVIEW BOOKING',
    maxSpeed: 'Max Speed',
    loadCapacity: 'Load Capacity',
    batteryCapacity: 'Battery Capacity',
    range: 'Range',
    SELECT: 'SELECT',

    //review booking string
    model: 'Model #',
    startTime: 'Start Time',
    endTime: 'End Time',
    pickupLocation: 'Pickup Location',
    rentalFare: 'Rental Fare',
    initialDeposit: 'Initial Deposit',
    totalPayAmt: 'Total Payable amount',

    //booking confirmed
    bookingConfirmed: "Booking Confirmed",
    shareLabel1: 'Share below mentioned code at the counter',
    bookAnotherRyde:'BOOK ANOTHER RYDE',
    cancelTheBooking:'Cancel This Booking',

    //my wallet screen
    myWallet:'My Wallet',
    totalBalance:"Total Balance",
    rydeBalance:"Ryde Balance",
    rewardBalance:"Reward Balance",
    addBALANCE:'ADD BALANCE',
    walletHistory:'Wallet History',
    date:'Date',
    transactionID:'Transaction ID',
    credit:'Credited',
    debit:'Debited'
}