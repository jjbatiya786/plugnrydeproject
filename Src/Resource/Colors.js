import { Appearance, useColorScheme } from "react-native";
const isDarkTheme = true//Appearance.getColorScheme()==="dark"

export const colors={
    backgroundColor:!isDarkTheme?'#FFFFFF': '#0A0D1A',
    black:'#000000',
    yellow:'#FFDE17',
    white:'#FFFFFF',
    colorGradientThemeWise:isDarkTheme?'#000000':'#FFFFFF',
    textInputBackGroundDark:'#141829',
    borderColor:'#FFFFFF1A',
    placeHolderTextColor:isDarkTheme?'rgba(255,255,255,0.4)':'rgba(0,0,0,0.4)',
    darkShadowLoginScreen:['#0A0D1A00','#0A0D1A'],
    lightShadowLoginScreen:['#FFFFFF00','#FFFFFF'],
    sendOtpBtnColor:'rgba(187,187,187,0.4)',
    blue:'#096DC9',
    headerColorLight:'#4A4A4A',
    horizontalLineDarkColor:'#FFFFFF4D',
    horizontalLineLightColor:'#E8E8E8',
    lightYellow:'rgba(255,222,23,0.3)',
    roundBorderColor:'#FFBE17',
    green:'#038B00',
    lightBlue:'#AEC0CA',
    lightThemeBlue:'rgba(174,192,202,0.8)',
    borderColorLineDark:'rgba(255,255,255,0.15)',
    modelBackGroundColor:'#FFFFFF26',
    horizontalLine:'#202020',
    tomato:'#C64343'

}