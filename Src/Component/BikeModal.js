import { Image, StyleSheet, Text, View, Dimensions, TouchableOpacity, Platform } from "react-native";
import Modal from "react-native-modal";
import { colors } from "../Resource/Colors";
import { images } from "../Resource/Images";
import { Fonts } from "../Resource/Fonts";
import { isDarkTheme } from "../Utilities/helper";
import { CommonString } from "../Resource/Strings";
import { useState } from "react";
import Slider from '@react-native-community/slider';
import RoundButton from "./RoundButton";
const { width } = Dimensions.get("screen")
const BikeModal = (props) => {
    const {isOpen,onCancelClick,onSelectBike}=props
    const [selectDurationIndex, setDurationIndex] = useState(0)
    const [sliderValue, setSliderValue] = useState(15);
    const headerView = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                <View style={styles.roundView1}>
                    <View style={styles.roundView2}>
                        <Text style={styles.txtId}>{"4"}</Text>
                    </View>
                </View>
                <View style={{ flex: 0.8 }}>
                    <Text style={styles.txtPlace}>{"Casbah"}</Text>
                    <Text numberOfLines={1} style={styles.txtAddress}>{"Next to Westin Hotel, HITEC...Next to Westin Hotel"}</Text>
                </View>
                <View style={styles.placeView}>
                    <Image style={styles.imgPlace} source={images.placeIcon} resizeMode={"contain"} />
                    <Text style={styles.txtNearTag}>{"800 m"}</Text>
                </View>
            </View>
        )
    }
    const roundViewButton = (index, label) => {
        return (
            <TouchableOpacity onPress={() => setDurationIndex(index)}
                style={selectDurationIndex === index ? styles.durationView : styles.durationUnselectView}
            >
                <Text style={selectDurationIndex === index ? styles.txtSelectLabel : styles.txtLabel}>{label}</Text>
            </TouchableOpacity>
        )
    }
    const onPress=()=>{
        onSelectBike()
    }
    return (
        <Modal
            isVisible={isOpen}
        >
            <View style={styles.modelView}>
                {headerView()}
                <View style={styles.horizontalView} />
                <Text style={styles.txtDuration}>{CommonString.duration}</Text>
                <View style={styles.viewDurationView}>
                    {roundViewButton(0, CommonString.strHour)}
                    {roundViewButton(1, CommonString.strWeek)}
                    {roundViewButton(2, CommonString.strMonth)}
                </View>
                <Text style={styles.txtDuration}>{CommonString.selectNumberOf}</Text>

                <Slider
                    style={{ height: 40, left: -3, width: "100%" }}
                    minimumValue={0}
                    maximumValue={24}
                    minimumTrackTintColor={colors.yellow}
                    thumbTintColor={colors.yellow}
                    maximumTrackTintColor={colors.modelBackGroundColor}
                    thumbImage={Platform.OS=="ios"?null: images.ellipseImg}
                    step={1}
                    onValueChange={
                        (sliderValue) => console.log(sliderValue)
                    }
                />
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 10 }}>
                    <Text style={styles.txtTrack}>{1}</Text>
                    <Text style={styles.txtTrack}>{6}</Text>
                    <Text style={styles.txtTrack}>{12}</Text>
                    <Text style={styles.txtTrack}>{18}</Text>
                    <Text style={styles.txtTrack}>{24}</Text>
                </View>
                <RoundButton
                    style={styles.roundView}
                    txt={CommonString.selectBike}
                    txtStyle={styles.txtStyle}
                    onPress={onPress}
                />
                <Text onPress={onCancelClick} style={styles.txtCancel}>{CommonString.cancel}</Text>
            </View>
        </Modal>
    )
}
const styles = StyleSheet.create({
    modelView: {
        backgroundColor: colors.backgroundColor, borderWidth: 1,
        borderColor: colors.modelBackGroundColor, borderRadius: 18,
        padding: 20,
    },
    //render styles
    roundView1: {
        height: 50, width: 50, borderRadius: 25, backgroundColor: colors.lightYellow,
        alignItems: 'center', justifyContent: 'center',
        marginRight: 10
    },
    roundView2: {
        height: 30, width: 30, borderRadius: 15, backgroundColor: colors.yellow,
        borderWidth: 1,
        borderColor: colors.roundBorderColor,
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtId: {
        fontFamily: Fonts.Arial,
        fontSize: 18,
        fontWeight: "700",
        color: colors.black
    },
    txtPlace: {
        fontFamily: Fonts.ApexNewMedium,
        fontSize: 18,
        fontWeight: "600",
        color: isDarkTheme ? colors.white : colors.black,
        lineHeight: 25,
        letterSpacing: 0.2,

    },
    txtAddress: {
        fontFamily: Fonts.ApexNew,
        fontSize: 16,
        fontWeight: "500",
        color: isDarkTheme ? colors.white : colors.black,
        lineHeight: 25,
        letterSpacing: 0.2,
    },
    imgPlace: {
        width: 14,
        height: 20,
        tintColor: isDarkTheme ? colors.green : undefined,
    },
    txtNearTag: {
        fontFamily: Fonts.Arial,
        fontSize: 14,
        fontWeight: "400",
        color: isDarkTheme ? colors.white : colors.black,
        lineHeight: 25,
        letterSpacing: 0.2,
    },
    placeView: {
        flex: 0.2, alignItems: 'center', justifyContent: 'flex-start', marginTop: 5
    },
    horizontalView: {
        height: 1,
        backgroundColor: colors.modelBackGroundColor,
        marginVertical: 10
    },
    txtDuration: {
        fontFamily: Fonts.ApexNewMedium,
        fontSize: 16,
        fontWeight: "500",
        color: isDarkTheme ? colors.white : colors.black,
        lineHeight: 25,
        letterSpacing: 0.2,
    },
    durationView: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.yellow,
        borderRadius: 40,
        paddingHorizontal: 24,
        paddingVertical: 12
    },
    durationUnselectView: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.black,
        borderRadius: 40,
        borderWidth: 1,
        borderColor: colors.modelBackGroundColor,
        paddingHorizontal: 24,
        paddingVertical: 12
    },
    txtLabel: {
        fontFamily: Fonts.ApexNewMedium,
        fontSize: 16,
        fontWeight: "500",
        color: isDarkTheme ? colors.white : colors.black,
        lineHeight: 25,
        letterSpacing: 0.4,
    },
    txtSelectLabel: {
        fontFamily: Fonts.ApexNewMedium,
        fontSize: 16,
        fontWeight: "500",
        color: colors.black,
        lineHeight: 25,
        letterSpacing: 0.4,
    },
    viewDurationView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 10
    },
    txtTrack: {
        fontFamily: Fonts.Arial,
        fontSize: 14,
        fontWeight: "400",
        color: colors.white,
        lineHeight: 25,
        letterSpacing: 0.4,
    },
    roundView: {
        height: 60,
        borderRadius: 40,
        backgroundColor: colors.yellow,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        width:'100%',
        marginTop:29

    },
    txtStyle: {
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: colors.black,
        letterSpacing: 0.4,
    },
    txtCancel:{
        fontFamily:Fonts.ApexNewMedium,
        fontSize: 16,
        lineHeight: 25,
        fontWeight: "500",
        color: colors.white,
        letterSpacing: 0.2,
        opacity:0.5,
        textAlign:"center",
        marginVertical:20,
    }
})
export default BikeModal;
