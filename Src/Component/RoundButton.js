import { Image, StyleSheet, Text, TouchableOpacity } from "react-native";

const RoundButton = (props) => {
    const { btnImgStyle, image, style, onPress,txt,txtStyle } = props
    return (
        <TouchableOpacity onPress={onPress} style={style}>
            {image != undefined ? <Image style={btnImgStyle} source={image} resizeMode={"contain"} /> :
                <Text style={txtStyle}>{txt}</Text>}

        </TouchableOpacity>
    )
}
export default RoundButton;