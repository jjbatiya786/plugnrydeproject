import { Image, StyleSheet, Text, View, Dimensions, TouchableOpacity } from "react-native";
import { isDarkTheme } from "../Utilities/helper";
import { colors } from "../Resource/Colors";
import { Fonts } from "../Resource/Fonts";
const { height, width } = Dimensions.get("screen")
const HeaderBack = (props) => {
    const { title, image, onBackPress, logoutTxt,marginRight } = props
    return (
        <View style={styles.mainView}>
            <TouchableOpacity onPress={onBackPress}>
                <Image style={styles.backImgView} source={image} resizeMode={"contain"} />
            </TouchableOpacity>
            <Text style={[styles.txtTitle,{marginRight:marginRight==undefined?0:marginRight}]}>{title}</Text>
            <TouchableOpacity>
                <Text style={styles.txtLogout}>{logoutTxt}</Text>
            </TouchableOpacity>
        </View>

    )
}
const styles = StyleSheet.create({
    mainView: {
        alignItems: 'center',
        marginTop: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20
    },
    txtTitle: {
        fontFamily: Fonts.ApexNewMedium,
        fontWeight: "500",
        fontSize: 20,
        // lineHeight: 25,
        letterSpacing: 0.2,
        color: isDarkTheme ? colors.white : colors.headerColorLight,
        alignSelf: 'center',
        textAlign: 'center',
        marginLeft: 20,
        textTransform:"capitalize"
    },

    backImgView: {
        height: 12,
        width: 15,
        tintColor: isDarkTheme ? colors.white : colors.black,
    },
    txtLogout: {
        fontFamily: Fonts.ApexNewMedium,
        fontWeight: "500",
        fontSize: 16,
        lineHeight: 25,
        letterSpacing: 0.4,
        color: colors.blue

    }
})
export default HeaderBack;