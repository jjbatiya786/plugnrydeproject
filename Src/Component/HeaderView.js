import { Image, StyleSheet, Text, View, Dimensions } from "react-native";
import { isDarkTheme } from "../Utilities/helper";
import { colors } from "../Resource/Colors";
import { Fonts } from "../Resource/Fonts";
const { height, width } = Dimensions.get("screen")
const HeaderView = (props) => {
    const { title } = props
    return (
        <View style={styles.mainView}>
            <Text style={styles.txtTitle}>{title}</Text>
            <View style={styles.horizontalLine} />
        </View>
    )
}
const styles = StyleSheet.create({
    mainView: {
        alignItems: 'center',
        marginTop: 30
    },
    txtTitle: {
        fontFamily:Fonts.ApexNewMedium,
        fontWeight: "500",
        fontSize: 20,
        lineHeight: 25,
        letterSpacing: 0.2,
        color: isDarkTheme ? colors.white : colors.headerColorLight,
        alignSelf: 'center',
        textAlign: 'center'
    },
    horizontalLine: {
        height: 1,
        backgroundColor: isDarkTheme ? colors.horizontalLineDarkColor : colors.horizontalLineLightColor,
        width: width,
        marginVertical: 20
    },
    backImgView: {
        height: 12,
        width: 15,
        tintColor: isDarkTheme ? colors.white : colors.black,
    }
})
export default HeaderView;