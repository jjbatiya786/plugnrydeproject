/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {
  Text,
  View,
} from 'react-native';
import RootStack from './Src/Navigation/RootStack';
const App = () => {
  return (
    <RootStack />
  )
}
export default App;
